using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class FireballShoot : MonoBehaviour
{
    private float _positionMultiplier = 0.005f;
    public GameObject PrefabFireball;
    private int _yPosition = 6;
    private float _timer;
    private float _time = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        _timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime; // Hay tiempo de espera entre cada lanzamiento
        Moviment();
        if (Input.GetKeyDown("space") && _timer > _time) GenerateFireBall();
    }

    void Moviment()
    {
        // Change Horitzontal Axis
        transform.position = new Vector3(_positionMultiplier * Input.GetAxis("Horizontal") + transform.position.x, transform.position.y, transform.position.z);
    }

    void GenerateFireBall()
    {
        _timer = 0;
        Instantiate(PrefabFireball, new Vector3(this.transform.position.x, _yPosition, 0), new Quaternion(0, 0, 0, 0));
        GameObject.Find("GameManager").GetComponent<GameManager>().FireBalls++;
    }
}
